const express = require("express");
const app = express();
const server = require("http").Server(app);
const dotenv = require("dotenv");
const mongoose = require("mongoose");

//Import Routes
const authRoute = require("./routes/auth");
const postRoute = require("./routes/post");

dotenv.config();

//Connect to DB
mongoose.connect(process.env.DB_CONNECT, {useNewUrlParser: true, useUnifiedTopology: true}, () => {
    console.log("Connected to db!");
});

//Middleware
app.use(express.json());

//Route Middlewares
app.use("/auth", authRoute);
app.use("/post", postRoute);

server.listen(3000, () => {
    console.log("Server up and running on 3000");
});
