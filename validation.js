const Joi = require("joi");
const options = {abortEarly: false};

//Register Validation
const registerValidation = data => {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        fullname: Joi.string().min(1).required(),
        username: Joi.string().min(6).required(),
        password: Joi.string().min(6).required()
    });
    return schema.validate(data, options);
};

//Login Validation
const loginValidation = data => {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    });
    return schema.validate(data, options);
};

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
